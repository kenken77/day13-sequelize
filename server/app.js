var Sequelize = require ("sequelize");
//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="password1234"
var connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Employees = require('./models/employees')(connection, Sequelize);
//console.log(Employees);
/*
Employees.findAll({limit: 10}).then((results)=>{
    console.log(results);
    results.forEach(function(element) {
        console.log(element.emp_no);
        console.log(element.birth_date);
        console.log(element.first_name);
        console.log(element.last_name);
        console.log(element.gender);
        console.log(element.hire_date);
        console.log("---------------------");
    }, this);
}).catch((error)=>{
    console.log(errror);
});

Employees.findOne({where: {emp_no: 2}}).then((result)=>{
    console.log("-----------find one----------");
    //console.log(result);
    console.log(result.emp_no);
    console.log(result.birth_date);
    console.log(result.first_name);
    console.log(result.last_name);
    console.log(result.gender);
    console.log(result.hire_date);
}).catch((error)=>{
    console.log(error);
})*/

/*
Employees.findOne({attributes: ['emp_no', 'gender', ['gender', 'Gender value']], where: {emp_no: 2}}).then((result)=>{

    console.log("-----------find one (Projection) ----------");
    //console.log(result);
    console.log(result.emp_no);
    console.log(result.gender);
    console.log(result.dataValues['Gender value']);
}).catch((error)=>{
    console.log(error);
})*/
/*
Employees.findOne({attributes: {exclude: 'gender'}, where: {emp_no: 2}}).then((result)=>{
        console.log("-----------find one (exclude) ----------");
        console.log(result.emp_no);
        console.log(result.birth_date);
        console.log(result.first_name);
        console.log(result.last_name);
        console.log("> Gender " + result.gender);
        console.log(result.hire_date);
}).catch((error)=>{
    console.log(error);
});
*/
/*
const Op = Sequelize.Op;
let queryCriteria = {   limit: 50, 
                        where: {
                                [Op.or]: [
                                        {first_name: {
                                            [Op.like]: 'Ken%'}
                                        },
                                        {first_name: {
                                            [Op.like]: 'Kel%'}
                                        }
                                    ]
                            }
                    };
Employees.findAll(queryCriteria).then((results)=>{
    console.log("-----------find one (OR condition) ----------");
    results.forEach(function(element) {
        if(element.first_name == 'Kelvin'){
            console.log(element.emp_no);
            console.log(element.birth_date);
            console.log(element.first_name);
            console.log(element.last_name);
            console.log(element.gender);
            console.log(element.hire_date);
            console.log("---------------------");
        }        
    }, this);
}).catch((error)=>{
    console.log(error);
})*/

/*
const Op = Sequelize.Op;
let queryCriteria = {   limit: 50, 
                        where: {
                                [Op.and]: [
                                        {first_name: {
                                            [Op.eq]: 'Kenneth'}
                                        },
                                        {last_name: {
                                            [Op.eq]: 'Tan'}
                                        }
                                    ]
                            }
                    };
Employees.findAll(queryCriteria).then((results)=>{
    console.log("-----------find all (AND condition) -> first name and last name ----------");
    results.forEach(function(element) {
            console.log(element.emp_no);
            console.log(element.birth_date);
            console.log(element.first_name);
            console.log(element.last_name);
            console.log(element.gender);
            console.log(element.hire_date);
            console.log("---------------------");
    }, this);
}).catch((error)=>{
    console.log(error);
})*/

/*
Employees.findAndCountAll({}).then((result)=>{
    console.log("-----------find all and count -----------");
    console.log(result.count);
}).catch((error)=>{
    console.log(error);
})*/

/*
Employees.findOne({where: {emp_no: 1}}).then((result)=>{
    result.update({last_name: 'Lee', gender:'F'}).then(()=>{
        console.log("record updated");
    });
}).catch((error)=>{
    console.log(error);
});*/

/*
Employees.findOne({where: {emp_no: 59}}).then((result)=>{
    result.destroy();
}).catch((error)=>{
    console.log(error);
});*/

Employees.findOrCreate({
    where: 
        {emp_no: 59}
    , defaults: {
        birth_date: new Date(),
        first_name: "Sarah",
        last_name: "Lee",
        gender: 'F',
        hire_date: new Date()
    }
}).spread((result, created)=>{
    //console.log(result);
    console.log(created);
});



