var Sequelize = require ("sequelize");
//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="password1234"

var connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

connection.query("SELECT * from employees LIMIT 10", 
        {type: connection.QueryTypes.SELECT})
        .then(employees =>{
            console.log(employees);
        });

connection.query("SELECT * from employees where last_name = :last_name LIMIT 10",
{ replacements: {last_name: 'Justin'}}, 
{type: connection.QueryTypes.SELECT})
.then(employees =>{
    console.log("------AND replacements------------")
    console.log(employees);
});

connection.query("SELECT * from employees where last_name like :last_name LIMIT 10",
{ replacements: {last_name: '%nng%'}}, 
{type: connection.QueryTypes.SELECT})
.then(function (res){
    console.log("--------LIKE replacements----------")
    console.log(res);
    res.forEach(function(element) {
        console.log(">>> " + element);
    }, this);
});

connection.query("UPDATE employees SET last_name = :last_name where emp_no = :emp_no",
{ replacements: {last_name: 'Tan', emp_no: 2}}, 
{type: connection.QueryTypes.UPDATE})
.spread(function (res, metadata){
    console.log("--------update employee record----------")
    console.log(res);
});

connection.query("INSERT INTO employees (emp_no, birth_date, first_name, last_name, gender, hire_date) VALUES (:emp_no, :birth_date, :first_name, :last_name, :gender, :hire_date)",
{ replacements: {emp_no: '52', birth_date: new Date(), first_name: 'New Person', last_name: 'surname', gender: 'M', hire_date: new Date()}
}, 
{type: connection.QueryTypes.INSERT})
.spread(function (res, metadata){
    console.log("--------insert employee record----------")
    console.log(res);
});

connection.query("DELETE FROM employees where emp_no = :emp_no",
{ replacements: {emp_no: '52'}}, 
{type: connection.QueryTypes.DELETE})
.spread(function (res, metadata){
    console.log("--------delete employee record----------")
    console.log(res);
});

//DELETE FROM employees where emp_no = :emp_no
